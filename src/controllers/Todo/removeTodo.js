const { PatchTodo } = require("../../services/todo.service");

const removeTodo = () => {
  return async (req, res, next) => {
    try {
      await PatchTodo.removeTodo(req.params.todoId);
      res.status(200).json({ message: "Todo has been removed" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = removeTodo;
