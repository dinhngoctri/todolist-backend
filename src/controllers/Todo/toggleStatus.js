const { PatchTodo } = require("../../services/todo.service");

const toggleStatus = () => {
  return async (req, res, next) => {
    try {
      await PatchTodo.toggleStatus(req.params.todoId);
      res.status(200).json({ message: "success" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = toggleStatus;
