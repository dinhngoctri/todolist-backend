const { GetTodo } = require("../../services/todo.service");

const getTodoById = () => {
  return async (req, res, next) => {
    try {
      const data = await GetTodo.getTodoById(req.params.id);
      res.status(200).json({ data, message: "Get todo success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getTodoById;
