const { GetTodo } = require("../../services/todo.service");

const getTodoByDate = () => {
  return async (req, res, next) => {
    try {
      const data = await GetTodo.getTodoByDate(req.query);
      res.status(200).json({ data, message: "Get todo list success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getTodoByDate;
