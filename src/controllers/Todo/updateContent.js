const { PatchTodo } = require("../../services/todo.service");

const updateContent = () => {
  return async (req, res, next) => {
    try {
      const data = await PatchTodo.updateContent(req.body);
      res.status(200).json({ data, message: "Update content success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = updateContent;
