const { PostTodo } = require("../../services/todo.service");

const createTodo = () => {
  return async (req, res, next) => {
    try {
      const data = await PostTodo.createTodo(req.body);
      res.status(200).json({ data, message: "New task added!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = createTodo;
