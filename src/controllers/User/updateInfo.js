const { PutUser } = require("../../services/user.service");

const updateInfo = () => {
  return async (req, res, next) => {
    try {
      await PutUser.updateInfo(req.body, req.params.userId);
      res.status(200).json({ message: "Update success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = updateInfo;
