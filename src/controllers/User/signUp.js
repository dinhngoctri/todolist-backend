const { PostUser } = require("../../services/user.service");

const signUp = () => {
  return async (req, res, next) => {
    try {
      const data = await PostUser.signUp(req.body);
      res.status(200).json({ data, message: "Sign up success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = signUp;
