const { PostUser } = require("../../services/user.service");

const confirmPassword = () => {
  return async (req, res, next) => {
    try {
      await PostUser.confirmPassword(req.body);
      res.status(200).json({ message: "Confirm success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = confirmPassword;
