const { PostUser } = require("../../services/user.service");

const signIn = () => {
  return async (req, res, next) => {
    try {
      const data = await PostUser.signIn(req.body);
      const { userId, username } = data;
      res.status(200).json({ data: { userId, username }, message: "Sign in success!" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = signIn;
