const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const todolist_v1 = require("./routers/todolist-v1.index");
app.use("/todolist-1.0", todolist_v1);

const { handleError } = require("./helpers/error");
app.use(handleError);

app.listen(4400);
