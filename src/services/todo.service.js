const { User, Todo } = require("../models/models.index");
const { AppError } = require("../helpers/error");
const { todoValidate } = require("../validations/todo.validate");
const { userValidate } = require("../validations/user.validate");

const GetTodo = {
  async getTodoByDate(payload) {
    try {
      const { userId, createdDate } = payload;
      await userValidate.isUserExist(userId);
      return await Todo.findAll({ where: { userId, createdDate, removed: false } });
    } catch (error) {
      throw error;
    }
  },

  async getTodoById(id) {
    try {
      return await Todo.findOne({ where: { id, removed: false } });
    } catch (error) {
      throw error;
    }
  },
};

const PostTodo = {
  async createTodo(todo) {
    try {
      const { title, createdDate, userId } = todo;

      if (!title.trim()) throw new AppError(400, "Missing todo title!");

      const date = new Date(createdDate);
      if (
        typeof createdDate !== "string" ||
        !(date instanceof Date && !isNaN(date.valueOf()))
      )
        throw new AppError(400, "createdDate must be 'YYYY-MM-DD'");

      await userValidate.isUserExist(userId);

      delete todo.status;
      return await Todo.create(todo);
    } catch (error) {
      throw error;
    }
  },
};

const PatchTodo = {
  async toggleStatus(id) {
    try {
      const todo = await todoValidate.isTodoExist(id);
      await Todo.update(
        { status: todo.status === "doing" ? "done" : "doing" },
        { where: { id } }
      );
    } catch (error) {
      throw error;
    }
  },

  async updateContent(content) {
    try {
      const { id, title, desc } = content;
      await todoValidate.isTodoExist(id);
      await Todo.update({ title, desc }, { where: { id } });
      return { title, desc };
    } catch (error) {
      throw error;
    }
  },

  async removeTodo(id) {
    try {
      await todoValidate.isTodoExist(id);
      await Todo.update({ removed: true }, { where: { id } });
    } catch (error) {
      throw error;
    }
  },
};

module.exports = { PostTodo, PatchTodo, GetTodo };
