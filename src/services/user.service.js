const { AppError } = require("../helpers/error");
const { User } = require("../models/models.index");
const { userValidate } = require("../validations/user.validate");

const PostUser = {
  async signIn(userInfo) {
    try {
      const { username, password } = userInfo;
      const errMessage = "Username or password is not correct!";

      if (!username.trim() || !password.trim())
        throw new AppError(400, "Username and password can not be blank!");
      const user = await userValidate.isUsernameExist(username, "fulfill", errMessage);
      await userValidate.isPasswordCorrect(user.password, password, errMessage);

      return user;
    } catch (error) {
      throw error;
    }
  },

  async signUp(userInfo) {
    try {
      const { username, password } = userInfo;
      if (!username.trim()) throw new AppError(400, "Username can not be blank!");
      if (!password.trim()) throw new AppError(400, "Password can not be blank!");
      await userValidate.isUsernameExist(username, "reject");

      return await User.create(userInfo);
    } catch (error) {
      throw error;
    }
  },

  async confirmPassword(userInfo) {
    try {
      const { userId, password } = userInfo;
      if (!password.trim()) throw new AppError(400, "Enter your password!");

      const user = await userValidate.isUserExist(userId);
      await userValidate.isPasswordCorrect(user.password, password);
    } catch (error) {
      throw error;
    }
  },
};

const PutUser = {
  async updateInfo(userInfo, userId) {
    try {
      await userValidate.isUserExist(userId);
      await userValidate.isUsernameExist(userInfo.username, "reject");

      return await User.update(userInfo, { where: { userId } });
    } catch (error) {
      throw error;
    }
  },
};

module.exports = { PostUser, PutUser };
