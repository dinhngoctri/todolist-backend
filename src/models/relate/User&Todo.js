module.exports = (User, Todo) => {
  Todo.belongsTo(User, { foreignKey: "userId" });
  User.hasMany(Todo, { foreignKey: "userId" });
};
