const { Sequelize } = require("sequelize");
const sequelize = new Sequelize("todolist", "root", "dinhtri2605", {
  dialect: "mysql",
  host: "localhost",
  port: 3307,
});

const User = require("./tables/User")(sequelize);
const Todo = require("./tables/Todo")(sequelize);

require("./relate/User&Todo")(User, Todo);

module.exports = { User, Todo };
