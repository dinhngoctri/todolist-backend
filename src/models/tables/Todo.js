const moment = require("moment");
const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Todo",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      desc: {
        type: DataTypes.STRING,
      },
      status: {
        type: DataTypes.ENUM("doing", "done"),
        allowNull: false,
        defaultValue: "doing",
      },
      createdDate: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        defaultValue: moment().format("YYYY-MM-DD"),
      },
      removed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      tableName: "todos",
      timestamps: false,
    }
  );
};
