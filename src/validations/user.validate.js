const { AppError } = require("../helpers/error");
const { User } = require("../models/models.index");
const bcrypt = require("bcrypt");

const userValidate = {
  async isUsernameExist(username, result, message = "Username already existed!") {
    try {
      const user = await User.findOne({ where: { username } });
      if (user && result === "fulfill") return user;
      if (!user && result === "fulfill") throw new AppError(404, message);
      if (user && result === "reject") throw new AppError(400, message);
      return user;
    } catch (error) {
      throw error;
    }
  },

  async isUserExist(userId, message = "User does not exist or has been deleted!") {
    try {
      const user = await User.findByPk(userId);
      if (!user) throw new AppError(404, message);
      return user;
    } catch (error) {
      throw error;
    }
  },

  async isPasswordCorrect(
    passwordDB,
    passwordInput,
    message = "Password is not correct!"
  ) {
    try {
      const isCorrect = bcrypt.compareSync(passwordInput, passwordDB);
      if (!isCorrect) throw new AppError(400, message);
    } catch (error) {
      throw error;
    }
  },
};

module.exports = { userValidate };
