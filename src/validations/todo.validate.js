const { AppError } = require("../helpers/error");
const { Todo } = require("../models/models.index");

const todoValidate = {
  async isTodoExist(todoId, message = "Todo does not exist or has been deleted!") {
    try {
      const todo = await Todo.findByPk(todoId);
      if (!todo) throw new AppError(404, message);
      return todo;
    } catch (error) {
      throw error;
    }
  },
};

module.exports = { todoValidate };
