const express = require("express");
const userRouter = require("./todolist-v1/user-v1");
const todoRouter = require("./todolist-v1/todo-v1");

const todolist_v1 = express.Router();
todolist_v1.use("", userRouter);
todolist_v1.use("", todoRouter);

module.exports = todolist_v1;
