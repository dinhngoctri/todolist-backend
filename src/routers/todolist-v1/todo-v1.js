const express = require("express");
const todoRouter = express.Router();

const createTodo = require("../../controllers/Todo/createTodo");
todoRouter.post("/createTodo", createTodo());

const toggleStatus = require("../../controllers/Todo/toggleStatus");
todoRouter.patch("/toggleStatus/todo-:todoId", toggleStatus());

const updateContent = require("../../controllers/Todo/updateContent");
todoRouter.patch("/updateContent", updateContent());

const removeTodo = require("../../controllers/Todo/removeTodo");
todoRouter.patch("/removeTodo/todo-:todoId", removeTodo());

const getTodoByDate = require("../../controllers/Todo/getTodoByDate");
todoRouter.get("/getTodoByDate", getTodoByDate());

const getTodoById = require("../../controllers/Todo/getTodoById");
todoRouter.get("/getTodoById/id-:id", getTodoById());

module.exports = todoRouter;
