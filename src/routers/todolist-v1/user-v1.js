const express = require("express");
const userRouter = express.Router();

const signUp = require("../../controllers/User/signUp");
userRouter.post("/signUp", signUp());

const signIn = require("../../controllers/User/signIn");
userRouter.post("/signIn", signIn());

const updateInfo = require("../../controllers/User/updateInfo");
userRouter.put("/updateInfo/user-:userId", updateInfo());

const confirmPassword = require("../../controllers/User/confirmPassword");
userRouter.post("/confirmPassword", confirmPassword());

module.exports = userRouter;
